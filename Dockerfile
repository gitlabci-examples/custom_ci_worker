# Sample Dockerfile to build a custom Jenkins slave image to be used
# with CERN Jenkins instances (cf. http://cern.ch/jenkinsdocs)

# Start from the base SLC6 or CC7 slave images
FROM gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
# Use tag 'slc6' instead of 'cc7' for SLC6
# The FROM statement can be overriden in the GitLab-CI build

# install custom packages (in this example, ghostscript and ghostscript-devel)
### TODO: set the list of packages as necessary
### Set ulimit lower as per
### https://gitlab.docs.cern.ch/docs/Build%20your%20application/CI-CD/Runners/TroubleShooting/yum-install-hang
RUN ulimit -n 1048576 && yum install -y ghostscript ghostscript-devel && yum clean all

